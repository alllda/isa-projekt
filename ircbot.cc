#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <arpa/inet.h>
#include <ifaddrs.h>
#include <list>
#include <sstream>
#include <time.h>

#define ERR_C -1
#define OK_C 1
#define HELP_C 2
#define BUFSIZE 4096
using namespace std;

typedef struct{
	string ircServer;
	int ircPort;
	string chanel;
	string syslogServer;
	list<string> wordsList;
	int code;
}Targument;


typedef struct{
	string prefix;
	string cmd;
	string param;
	string trailing;
	int code;
}TParsedMsg;

typedef struct{
	unsigned char a,b,c,d;
}TIPv4;


void printHelp(){
	cout << endl;
	cout << "IRC bot with syslog logging" << endl
		<<  "---------------------------" << endl
		<<  "- bot scan all PRIVMSG or NOTICE msg from irc server and" << endl
		<<  "  looking for words in wordList(argument), than send log" << endl
		<<  "  message to syslog server" << endl << endl
		<<  "Launch:" << endl
		<<  "  ./ircbot IRCserver[:IRCport] IRCchannel syslogServer [wordList]" << endl
		<<  "       - wordlist = words with delimiter \";\"" << endl << endl;
}

Targument parseArgument(char **argv, int argc){
	Targument a;
	if(argc == 2 && (strcmp(argv[1],"--help") == 0 || strcmp(argv[1],"-h") == 0)){
		a.code = HELP_C;
		return a;
	}
	if(argc < 4 || argc > 5){ // bad count of argument
		a.code = ERR_C;
		return a;
	}
	string first(argv[1]);
	if (first.find(":") != string::npos){ // first argument contain port
		a.ircServer = first.substr(0,first.find(":"));
		a.ircPort = atoi(first.substr(first.find(":")+1).c_str());
	}
	else{ // without port..use default 6667
		a.ircServer = first;
		a.ircPort = 6667;
	}
	a.chanel = string(argv[2]);
	a.syslogServer = string(argv[3]);
	if(argc == 5){ // words
		size_t pos = 0;
		string words(argv[4]);
		string token;
		string delimiter = ";";
		while ((pos = words.find(delimiter)) != std::string::npos) {
		    token = words.substr(0, pos);
		    a.wordsList.push_back(token);
		    words.erase(0, pos + delimiter.length()); //erase word from bigining
		}
		a.wordsList.push_back(words);
	}
	/*for(list<string>::const_iterator i = a.wordsList.begin(); i != a.wordsList.end(); ++i)
	{
	     printf("%s\n", i->c_str());
	}*/
	a.code = OK_C;
	return a;
}
/**
 * clear buffer
 * @param buf     buffer
 * @param bufSize size of buffer
 */
void removeBuffer(char * buf,int bufSize){
	for(int i = 0; i< bufSize;i++)
		buf[i] = '\0';
}

class IRC
{
	string serverName;
	int port;
	bool autopong;
	int mysocket;
	struct sockaddr_in sin;
	struct hostent *server;
	int bufferSize = BUFSIZE;
public:
	IRC(string server, int port = 6667, bool autopong = true){
		this->serverName = server;
		this->port = port;
		this->autopong = autopong;
	}

	/**
	 * Connect to server
	 * @param  user userName
	 * @return      ERR_C/OK_C
	 */
	int myConnect(string user){

		if((server = gethostbyname(this->serverName.c_str())) ==NULL) {
			cerr << "Cant get host by name IRC" << endl;
			return ERR_C;
		}
		//create socket
		if((mysocket = socket(PF_INET, SOCK_STREAM, 0)) < 0){
			cerr << "Cant create socket" << endl;
			return ERR_C;
		}

		bzero(&sin,sizeof(sin));
		sin.sin_family = PF_INET;
		sin.sin_port = htons(this->port);
		memcpy(&sin.sin_addr,server->h_addr,server->h_length);

		// connect to server
		if((connect(mysocket,(struct sockaddr *)&sin,sizeof(sin))) < 0){
			cerr << "Cant connect to server" << endl;
			return ERR_C;
		}
		return this->logIn(user);		

	}
	// send msg to irc server
	int sendMsg(const char * msg){
		if((send(this->mysocket,msg,strlen(msg), 0 )) < 0){
			return ERR_C;
		}
		return OK_C;
	}

	/**
	 * log in user with username
	 * @param  user username
	 * @return      ERR_C/OK_C
	 */
	int logIn(string user){
		//password
		string passMessage = "PASS none\r\n";
		if(this->sendMsg(passMessage.c_str()) == -1){
			cerr << "Cant send password" << endl;
			return ERR_C;
		}
		// User
		string userMessage = "USER "+ user + " " + user + " " + user + " " + user + "\r\n";
		if(this->sendMsg(userMessage.c_str()) == -1){
			cerr << "Cant send USER" << endl;
			return ERR_C;
		}
		// Nick
		string nickMessage = "NICK "+ user +"\r\n";
		if(this->sendMsg(nickMessage.c_str()) == -1){
			cerr << "Cant send NICK" << endl;
			return ERR_C;
		}

		return OK_C;
	}

	/**
	 * Join to chanel
	 * @param  chanel channel name
	 * @return        ERR_C/OK_C
	 */
	int joinChanel(string chanel){
		string joinMsg = "JOIN " +chanel +"\r\n";
		return this->sendMsg(joinMsg.c_str());
	}

	/**
	 * Recive message from server
	 * @param  buf buffer for incoming message
	 * @return     ERR_C/OK_C
	 */
	int recive(char *buf, int buffsize){
		if(read(this->mysocket,buf,buffsize) < 0){
			cerr << "Read error" << endl;
			return ERR_C;
		}
		// reply PONG msg
		if(this->autopong && strncmp(buf,"PING",4) ==0){
			buf[1] = 'O'; // Replace PING with PONG
			if(this->sendMsg(buf) == ERR_C){
				cerr << "Cant send PONG" << endl;
				return ERR_C;
			}
		}
		return OK_C;
	}

	/**
	 * Parse incoming message
	 * @param  msg message
	 * @return     TParseMessage structure
	 */
	TParsedMsg parseMsg(string msg){
		TParsedMsg parsed;
		parsed.prefix = "";
		parsed.cmd = "";
		parsed.param = "";
		parsed.trailing = "";
		parsed.code = 1;
		if (msg.at(0) == ':'){ // has message prefix (prefix start with ":")
			int firstSpace = msg.find(" "); //space is delimiter
			parsed.prefix = msg.substr(0,firstSpace);
			parsed.cmd = msg.substr(firstSpace+1, msg.substr(firstSpace+1).find(" "));
			msg.erase(0,parsed.prefix.length()+parsed.cmd.length()+2); //errase prefix and cmd
		}
		else{	//without prefix, only cmd
			int firstSpace = msg.find(" ");
			parsed.cmd = msg.substr(0,firstSpace);
			msg.erase(0, parsed.cmd.length()+1);
		}
		if (msg != ""){	
			if(msg.find(":") != std::string::npos){	//message has trailing
				int doubleDot = msg.find(":"); 
				parsed.param = msg.substr(0,doubleDot);
				parsed.trailing = msg.substr(doubleDot+1);
			}
			else{
				parsed.param = msg;
			}
		}
		return parsed;

	}
	~IRC();
	
};

class Syslog
{
	string server;
	int port = 514;
	int mysocket;
	struct hostent *hp;     /* host information */
	struct sockaddr_in servaddr;    /* server address */
public:
	Syslog(string server){
		this->server = server;
		
	}

	/**
	 * Connect to syslog server
	 * @return ERR_C/OK_C
	 */
	int connect(){
		if ((this->mysocket = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
			return ERR_C;
		}
		memset((char*)&servaddr, 0, sizeof(servaddr));
		servaddr.sin_family = AF_INET;
		servaddr.sin_port = htons(this->port);
		hp = gethostbyname(this->server.c_str());
		if (!hp) {
			return ERR_C;
		}

		memcpy((void *)&servaddr.sin_addr, hp->h_addr_list[0], hp->h_length);
		
		return OK_C;
	}

	/**
	 * Send message to syslog server
	 * @param  msg message
	 * @return     ERR_C/OK_C
	 */
	int sendMsg(string msg){
		if(sendto(this->mysocket,msg.c_str(),msg.length()-1, 0, 
			(struct sockaddr *)&this->servaddr, sizeof(this->servaddr)) < 0)
			return ERR_C;
		cout <<msg << endl;
		return OK_C;
	}

	/**
	 * Get timestamp for syslog message in format
	 * @param buffer buffer for timestamp
	 * @param length length of buffer
	 */
	void getTimestamp(char * buffer, int length){
		time_t rawtime;
		struct tm * timeinfo;

		time (&rawtime);
		timeinfo = localtime (&rawtime);
		// format time
		strftime (buffer,length,"%a %d %H:%M:%S",timeinfo);
		if(buffer[4] == '0'){
			buffer[4] = ' ';
		}
	}

	/**
	 * Overload send mesage 
	 * @param  address     IP addres of host
	 * @param  programName program name - ircbot
	 * @param  nickName    nickaname of sender
	 * @param  message     message
	 * @return             ERR_C/OK_C
	 */
	int sendMsg(string address, string programName, string nickName, string message){
		char timestamp[80];
		getTimestamp(timestamp, 80);
		string msg = string("<134>"+ string(timestamp) +" "+ address +" "+programName + " <" + nickName +">: "+ message);
		sendMsg(msg);
		return OK_C;
	}

	~Syslog();
	
};

list<string> splitToLines(char *buffer){
	list<string> lines;
	size_t pos = 0;
	string buf(buffer);
	string token;
	string delimiter = "\n";
	while ((pos = buf.find(delimiter)) != std::string::npos) {
	    token = buf.substr(0, pos);
	    //std::cout << "Token: "<<token << std::endl;
	    lines.push_back(token);
	    buf.erase(0, pos + delimiter.length());
	}

	/*for(list<string>::const_iterator i = lines.begin(); i != lines.end(); ++i)
	{
	     printf("LINE: %s\n", i->c_str());
	}*/
	return lines;
}

/**
 * Check if str contain only ascii numbers
 * @param  str string
 * @return     true/false
 */
bool isNumber(string str){
	for(unsigned int i = 0; i < str.length();i++){
		if(!isdigit(str.at(i)))
			return false;
	}
	return true;
}

string getNickname(string prefix){
	if(prefix.length() < 1){
		return string("");
	}
	else{
		string pref = prefix.substr(1);
		if(pref.find("!") != string::npos)
			return pref.substr(0,pref.find("!"));
		else if(pref.find("@") != string::npos)
			return pref.substr(0,pref.find("@"));
		return pref;
	}
}

string getAddress(string prefix){
	if(prefix.length() < 1)
		return string("");
	else{
		if(prefix.find("@") != string::npos){
			int from = prefix.find("@") + 1;
			return prefix.substr(from);
		}
		else
			return string("");
	}
}

void printParsedMsg(TParsedMsg parsedMsg){
	cout << 
     "prefix :" << parsedMsg.prefix <<endl << 
     "cmd :" << parsedMsg.cmd << endl <<
     "param :" << parsedMsg.param << endl <<
     "trailing :" << parsedMsg.trailing << endl;
}

int  getMyIP(char * host)
{
	//http://stackoverflow.com/questions/2283494/get-ip-address-of-an-interface-on-linux
	struct ifaddrs *ifaddr, *ifa;
    int family, s;

    if (getifaddrs(&ifaddr) == -1) 
    {
        return ERR_C;
    }


    for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) 
    {
        if (ifa->ifa_addr == NULL)
            continue;  

        s=getnameinfo(ifa->ifa_addr,sizeof(struct sockaddr_in),host, NI_MAXHOST, NULL, 0, NI_NUMERICHOST);

        if(!(strcmp(ifa->ifa_name,"lo")==0)&&(ifa->ifa_addr->sa_family==AF_INET))
        {
            if (s != 0)
            {
                printf("getnameinfo() failed: %s\n", gai_strerror(s));
                exit(EXIT_FAILURE);
            }
            printf("\tInterface : <%s>\n",ifa->ifa_name );
            printf("\t  Address : <%s>\n", host); 
        }
    }

    freeifaddrs(ifaddr);
    return OK_C;
}

int main( int argc, char **argv){
	//getMyIP();
	Targument arg = parseArgument(argv, argc);
	if(arg.code == ERR_C){
		cerr << "Bad argument" << endl;
		return ERR_C;
	}
	if(arg.code == HELP_C){
		printHelp();
		return 0;
	}
	string user("xraszk03");
	IRC *irc = new IRC(arg.ircServer, arg.ircPort);
	if(irc->myConnect(user) == ERR_C){
		cerr << "IRC connection error " << endl;
		return ERR_C;
	}
	if(irc->joinChanel(arg.chanel) == ERR_C){
		cerr << "IRC join error " << endl;
		return ERR_C;
	}

	Syslog * syslog = new Syslog(arg.syslogServer);
	if(syslog->connect() == ERR_C){
		cerr << "Syslog connection error" << endl;
		return ERR_C;
	}
	char buffer[BUFSIZE];
	removeBuffer(buffer,BUFSIZE);
	list<string> lines;
	TParsedMsg parsedMsg;
	char ipv4[NI_MAXHOST];
	while(true){
		if(irc->recive(buffer, BUFSIZE) == ERR_C){
			cerr << "IRC reading error" << endl;
			return ERR_C;
		}
		//cout <<"[IN]:"<< buffer << endl;
		lines = splitToLines(buffer);
		for(list<string>::const_iterator i = lines.begin(); i != lines.end(); ++i)
		{
		     parsedMsg = irc->parseMsg(string(i->c_str()));
		     cout << "[IN]:" << i->c_str()<< endl;
		     //printParsedMsg(parsedMsg);
		     if(isNumber(parsedMsg.cmd)){
		     	if(atoi(parsedMsg.cmd.c_str()) >= 400 && atoi(parsedMsg.cmd.c_str()) <= 599){
		     		syslog->sendMsg("",string("ircbot"),"",string(i->c_str()));
		     		return ERR_C;
		     	}
		     }
		     else if(parsedMsg.cmd == "PRIVMSG" || parsedMsg.cmd == "NOTICE"){ // PRIVMSG or NOTICE message
		     	// searching for substring in incoming message
		     	for(list<string>::const_iterator a = arg.wordsList.begin(); a != arg.wordsList.end(); a++){
		     		string word(a->c_str());
		     		if(parsedMsg.trailing.find(word) != string::npos){
		     			getMyIP(ipv4);
		     			string address(ipv4);
		     			//string address = getAddress(parsedMsg.prefix);
		     			string nickName = getNickname(parsedMsg.prefix);
		     			syslog->sendMsg(address,string("ircbot"),nickName,parsedMsg.trailing);
		     		}
		     	}
		     }
		}
		removeBuffer(buffer,BUFSIZE);
	}

	return OK_C;

}