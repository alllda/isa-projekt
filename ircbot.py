import sys
import socket
import re
import time
import datetime

ircServer = ""
ircPort = 6667
ircChanel = ""
syslogServer = ""
words = []

def parseArg():
	global ircServer, ircPort, ircChanel, syslogServer, words
	print len(sys.argv)
	print sys.argv
	if len(sys.argv) < 4 or len(sys.argv) > 5:
		return False
	if ":" in sys.argv[1]:
		splited = sys.argv[1].split(":")
		ircServer = splited[0]
		ircPort = int(splited[1])
	else:
		ircServer = sys.argv[1]
	ircChanel = sys.argv[2]
	syslogServer = sys.argv[3]
	if len(sys.argv) == 5:
		if ";" in sys.argv[4]:
			words = sys.argv[4].split(";")
		else:
			words.append(sys.argv[4])
	return True

class IRC(object):
	"""docstring for IRC"""
	def __init__(self, server, port=6667, autopong=True):
		self.server = server
		self.port = port
		self.BufferSize = 4096
		self.autopong = autopong
		self.ircSock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

	def connect(self, user):
		self.ircSock.connect((self.server, self.port))
		self.ircSock.send("PASS none\n")
		self.ircSock.send("USER "+user+" "+user+" "+user+" "+user+"\n")
		self.ircSock.send("NICK "+user+"\n")
	
	def recive(self):
		message = self.ircSock.recv(self.BufferSize)
		if self.autopong:
			if re.match(r'^PING',message):
				self.sendMsg(message.replace('PING', 'PONG'))
				
		return message
	def sendMsg(self,msg):
		self.ircSock.send(msg+"\n")
	def joinChannel(self,chanel):
		self.ircSock.send("JOIN #"+chanel+"\n")
	def parseMsg(self, msg):
		output = {}
		output["prefix"] = None
		output["cmd"] = None
		output["param"] = None
		output["trailing"] = None
		others = ""
		if msg.startswith(":"):
			output["prefix"] = msg.split(" ")[0].strip()
			output["cmd"] = msg.split(" ")[1].strip()
			
			if(len(output["prefix"])+len(output["cmd"])+2 < len(msg)):
				others = msg[len(output["prefix"])+len(output["cmd"])+2:]

		else:
			output["cmd"] = msg.split(" ")[0].strip()
			if len(output["cmd"]) < len(msg):
				others = msg[len(output["cmd"]):]
				
		if others != "":
			if ":" in others:
				output["param"] = others.split(":")[0].strip()
				output["trailing"] = others.split(":")[1].strip()
			else:
				output["param"] = others

		return output

class Syslog(object):
	"""docstring for Syslog"""
	FACILITY = {
		'kern': 0, 'user': 1, 'mail': 2, 'daemon': 3,
		'auth': 4, 'syslog': 5, 'lpr': 6, 'news': 7,
		'uucp': 8, 'cron': 9, 'authpriv': 10, 'ftp': 11,
		'local0': 16, 'local1': 17, 'local2': 18, 'local3': 19,
		'local4': 20, 'local5': 21, 'local6': 22, 'local7': 23,
	}

	SEVERITY = {
		'emerg': 0, 'alert':1, 'crit': 2, 'err': 3,
		'warning': 4, 'notice': 5, 'info': 6, 'debug': 7
	}
	def __init__(self, server, port = 514):
		self.server = server
		self.port = port
		self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

	def sendMsg(self, msg, facility = FACILITY["local0"], level = SEVERITY["info"]):
		fullMsg = "<%d>%s: %s" % (level + facility*8,self.getTimestamp(), msg)
		print fullMsg
		self.socket.sendto(fullMsg,(self.server,self.port))
	def getTimestamp(self):
		ts = time.time()
		st = datetime.datetime.fromtimestamp(ts).strftime("%b %d %H:%M:%S")
		return st
		
		

def main():
	global ircServer, ircPort, ircChanel, syslogServer, words
	if (not parseArg()):
		sys.stderr.write("Wrong argument\n")
		sys.exit(-1)
	print "IRC server: " + ircServer
	print "IRC port: " + str(ircPort)
	print "IRC chanel: " + ircChanel
	print "Syslog server: " + syslogServer
	print "Words: " + str(words)
	irc = IRC(ircServer,ircPort)
	irc.connect("alojs")
	irc.joinChannel(ircChanel)
	mysyslog = Syslog("192.168.0.125")

	while True:
		msg = irc.recive()
		if not msg:
			break
		lines = msg.splitlines()
		for l in lines:
			print l
			parsed = irc.parseMsg(l)
			if parsed["trailing"]:
				for w in words:
					if w in parsed["trailing"]:
						mysyslog.sendMsg(l)

if __name__ == '__main__':
	main()