CPPFLAGS=-std=c++11 -pedantic -Wall

ircbot: ircbot.cc
	g++ $(CPPFLAGS) ircbot.cc -o $@

clean:
	rm ircbot